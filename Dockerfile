FROM ubuntu:14.04
MAINTAINER Azat Kurbanov <cordalace@gmail.com>
ADD dist/synantis*.egg /tmp/pkg.egg
ADD requirements.txt /tmp/requirements.txt
RUN apt-get update
RUN apt-get install -y python-dev python-virtualenv
RUN virtualenv --no-site-packages /opt/synantis
RUN /opt/synantis/bin/pip install pip --upgrade
RUN /opt/synantis/bin/pip install -r /tmp/requirements.txt
RUN /opt/synantis/bin/easy_install /tmp/pkg.egg
CMD /opt/synantis/bin/synantis-server -p 8080
