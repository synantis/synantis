"""
Application base
"""

from flask import Flask, Blueprint
from flask_registry import Registry, PackageRegistry
from flask_registry import ExtensionRegistry
from flask_registry import ConfigurationRegistry
from flask_registry import BlueprintAutoDiscoveryRegistry

from synantis import settings


class Config(object):
    """
    Application config
    """
    # EXTENSIONS = ['tests.mockext']
    # USER_CFG = True

    def __init__(self):
        self.PACKAGES = settings.PACKAGES[:]


def create_app(config):
    """
    Flask application factory
    """
    app = Flask('synantis')
    app.config.from_object(config)
    registry = Registry(app=app)
    registry['packages'] = PackageRegistry(app)
    registry['extensions'] = ExtensionRegistry(app)
    registry['config'] = ConfigurationRegistry(app)
    registry['blueprints'] = BlueprintAutoDiscoveryRegistry(app=app)

    return app, registry
