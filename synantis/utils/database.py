"""
Database utils
"""

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base

# global cache for sqlalchemy database engine and session factory
_CACHE = {}

BASE = declarative_base()


def init_schema():
    """
    Initialize database schema
    """
    BASE.metadata.create_all(bind=_CACHE['engine'])


def prepare_database(database_uri):
    """
    Create sqlalchemy engine and session factory
    """
    engine = create_engine(database_uri)
    session = scoped_session(sessionmaker(bind=engine))
    _CACHE["engine"] = engine
    _CACHE["session"] = session


def current_session():
    return _CACHE["session"]


def import_models(registry):
    for pkg in registry["packages"]:
        try:
            __import__(pkg + ".models")
        except ImportError:
            pass  # TODO: show warning
