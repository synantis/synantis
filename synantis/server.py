"""
Server global entities
"""

from synantis.utils import database as db, application as app_utils

CONFIG = app_utils.Config()
APP, REGISTRY = app_utils.create_app(CONFIG)


@APP.teardown_appcontext
def shutdown_db_session(exception=None):
    session = db.current_session()
    session.remove()
