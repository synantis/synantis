"""
Synantis settings
"""

PACKAGES = [
    'synantis.app.events',
    'synantis.app.users',
]
