"""
Argument parsing
"""

from __future__ import unicode_literals

import argparse


def init_argument_parser():
    """
    Initialize and return argument parser.

    :return: argument parser
    :rtype: argparse.ArgumentParser
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-l", "--listen",
        metavar="HOST",
        default="0.0.0.0",
        help="listen host",
    )
    parser.add_argument(
        "-p", "--port",
        type=int,
        default=8080,
        help="listen port",
    )
    parser.add_argument(
        "--no-init-db",
        action="store_true",
        help="do not create database schema if it is not created yet",
    )
    parser.add_argument(
        "--database",
        default="sqlite://",
        help="database connection string URI in SQLAlchemy format",
    )
    parser.add_argument(
        "-d", "--debug",
        action="store_true",
        help="turn on server debugging",
    )
    return parser


def get_parsed_args(unparsed_args=None):
    """
    Return parsed aruments.

    By default, the argument strings are taken from `sys.argv`.

    :param unparsed_args: arguments
    :type unparsed_args: sequence or None
    :return: populated namespace
    :rtype: argparse.Namespace
    """
    parser = init_argument_parser()
    args = parser.parse_args(unparsed_args)
    if isinstance(args.listen, str):
        args.listen = unicode(args.listen, 'utf-8')
    if isinstance(args.database, str):
        args.database = unicode(args.database, 'utf-8')
    return args
