"""
Test argument parsing
"""

from nose.tools import assert_equal, assert_is_instance, assert_true, \
    assert_false

from synantis.argument_parsing import init_argument_parser, get_parsed_args


def _parse_args(args_string):
    """
    Shortcut for `get_get_parsed_args` call
    :param args_string: arguments string
    :return: result of `get_parsed_args`
    """
    return get_parsed_args(args_string.split())


class TestParsing(object):
    """
    Test argument parsing
    """
    def test_listen(self):
        """
        Test listen argument
        """
        assert_equal("0.0.0.0", _parse_args("").listen)
        assert_equal("1.2.3.4", _parse_args("--listen=1.2.3.4").listen)
        assert_equal("1.2.3.4", _parse_args("-l 1.2.3.4").listen)
        assert_is_instance(_parse_args(str("-l 1.2.3.4")).listen, unicode)
        assert_is_instance(_parse_args(unicode("-l 1.2.3.4")).listen, unicode)

    def test_port(self):
        """
        Test port argument
        """
        assert_equal(8080, _parse_args("").port)
        assert_equal(1234, _parse_args("--port=1234").port)
        assert_equal(1234, _parse_args("-p 1234").port)

    def test_debug(self):
        """
        Test debug argument
        """
        assert_false(_parse_args("").debug)
        assert_true(_parse_args("--debug").debug)
        assert_true(_parse_args("-d").debug)

    def test_no_init_db(self):
        """
        Test no-init-db argument
        """
        assert_false(_parse_args("").no_init_db)
        assert_true(_parse_args("--no-init-db").no_init_db)

    def test_database(self):
        """
        Test database argument
        """
        assert_equal("sqlite://", _parse_args("").database)
        assert_equal(
            "postgresql://scott:tiger@localhost/mydatabase",
            _parse_args(
                "--database=postgresql://scott:tiger@localhost/mydatabase",
            ).database,
        )
        assert_is_instance(_parse_args(str(
            "--database=postgresql://scott:tiger@localhost/mydatabase",
        )).database, unicode)
        assert_is_instance(_parse_args(unicode(
            "--database=postgresql://scott:tiger@localhost/mydatabase",
        )).database, unicode)
