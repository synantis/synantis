"""
User views
"""

from flask import Blueprint
from flask import render_template

from synantis.app.users.models import User
from synantis.utils.database import current_session

blueprint = Blueprint('users', __name__, template_folder='templates')


@blueprint.route('/user')
def user():
    """
    User
    """
    session = current_session()
    import random
    name = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for i in xrange(8))
    u = User(name=name)
    session.add(u)
    session.commit()
    return render_template('users/user.html', name=name)
