from sqlalchemy import Column, Integer, String

from synantis.utils import database as db


class User(db.BASE):
    """
    User
    """
    __tablename__ = "user"
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)

    def __repr__(self):
        return '<User id=%(id)s name=%(name)s>' % {
            'id': self.id,
            'name': self.name,
        }
