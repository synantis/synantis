"""
Event views
"""

from flask import Blueprint
from flask import render_template

blueprint = Blueprint('events', __name__, template_folder='templates')


@blueprint.route('/event')
def event():
    """
    Event
    """
    return render_template('events/event.html', x=42)
