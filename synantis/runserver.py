"""
Standalone debug server run script

Some shit
"""

import sys
reload(sys).setdefaultencoding('UTF-8')


def main():
    """
    Main function
    """
    from synantis import server
    from synantis import argument_parsing
    from synantis.utils import database
    args = argument_parsing.get_parsed_args()
    database.import_models(server.REGISTRY)
    database.prepare_database(args.database)
    if not args.no_init_db:
        database.init_schema()
    server.APP.run(
        host=args.listen,
        port=args.port,
        debug=args.debug,
    )
    return 0

if __name__ == '__main__':
    sys.exit(main())
